import random as r
import datetime

#default variable and stuff
fileName = "generated_input.txt"
fileMode = "w" #for writing
textList = []
ratingMax = 10

#INIT default data
uniqueName = set()
uniqueLocation = set()
uniqueWebsiteName = set()
uniqueLoremIpsum = set()
uniqueIP = set()

#INIT row count for each table mentioned, made for clarity
universitasCount = 50
beritaCount = 100
ratingCount = 125
riwayatCount = 200
tagCount = 20
narasumberCount = 150
narasumber_beritaCount = 100
honorCount = 200
dosenCount = 25
stafCount = 50
MahasiswaCount = 100
RekeningCount = 300
kuponCount = 5
komentarCount = 150
pollingCount = 30
polling_beritaCount = 20
polling_biasaCount = 10
responCount = 40
respondenCount = 40

#Utils function
def randomString(length):
    stringSelection = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    stringRet = ""
    for i in range(0,length):
        stringRet += stringSelection[r.randint(0,len(stringSelection)-1)]
    return stringRet

def addAphostrophe(addThis):
    return "'" + addThis + "'"

def randomTimeStamp(start, delta):
    dateNow = datetime.datetime.now() + datetime.timedelta(minutes = r.randint(0, delta))
    return dateNow.strftime("%m-%d-%y")

def randomTime(delta):
    dateNow = datetime.datetime.now() + datetime.timedelta(minutes = r.randint(0, delta))
    return dateNow.strftime("%H:%M:%S")

def randomEmailName(name): #Dependent on randomWebsiteName
    return name.split(" ")[0] + "@" + r.choice(list(uniqueWebsiteName))

def generateRowsToString(textList,tableClass, rowsCount):
    a = None
    for i in range(rowsCount):
        a = tableClass()
        textList.append(str(a))
    print("made " + str(rowsCount) + " " + a.__class__.__name__ + " rows.")

def randomLoremIpsum(maxChar): #Dependent on uniqueLoremIpsum
    lorem = r.choice(list(uniqueLoremIpsum))
    if len(lorem) > maxChar:
        lorem = list(lorem)[:maxChar]
    return "".join(lorem)

def stringifyTable(tableName, *columns):
    theString = []
    theString.append("insert into ")
    theString.append(tableName)
    theString.append(" values (")

    for c in columns:
        if type(c) is str:
            c = addAphostrophe(c)
        else:
            c = str(c)
        theString.append(c)
        theString.append(",")
    del theString[-1]

    theString.append(");\n")
    return "".join(theString)

#class definition
class universitas: #FINAL
    universitasKeyID = set()
    tableName = "universitas"
    
    def __init__(self):
        self.id = self.generateID()
        universitas.universitasKeyID.add(self.id)
        
        self.jalan = r.choice(list(uniqueLocation))
        self.kelurahan = r.choice(list(uniqueLocation))
        self.provinsi = r.choice(list(uniqueLocation))
        self.kodepos = str(r.randint(10000,99999))
        self.website = "universitas." + r.choice(list(uniqueWebsiteName))
    
    def generateID(self):
        genID = r.randint(1,2000000)
        while genID in universitas.universitasKeyID:
            genID = r.randint(1,2000000)
        return genID
    
    def __str__(self):
        return stringifyTable(universitas.tableName, self.id, self.jalan, self.kelurahan, self.provinsi, self.kodepos, self.website)

class berita: #FINAL
    startMinuteMark = 3600
    beritaKeyURL = set()
    tableName = "berita"

    def __init__(self):
        self.url = self.generateID()
        berita.beritaKeyURL.add(self.url)

        self.judul = randomLoremIpsum(90)
        self.topik = randomLoremIpsum(90)
        self.created_at = randomTimeStamp(0, berita.startMinuteMark)
        self.updated_at = randomTimeStamp(berita.startMinuteMark, berita.startMinuteMark * 10) #need to be older than created_at
        self.jumlah_kata = r.randint(1, 5000) #arbitrary number
        self.rerata_rating = round(r.uniform(0,ratingMax), 2)
        self.id_universitas = r.choice(list(universitas.universitasKeyID)) #foreign key to universitas
    
    def generateID(self):
        genID = randomString(40)
        while genID in berita.beritaKeyURL:
            genID = randomString(40)
        return genID
    
    def __str__(self):
        return stringifyTable(berita.tableName, self.url, self.judul, self.topik, self.created_at, self.updated_at, self.jumlah_kata, self.rerata_rating, self.id_universitas)

class narasumber: #FINAL - ISH
    tableName = "narasumber"
    narasumberKeyID = set()

    def __init__(self):
        self.id = self.generateID() #PK
        narasumber.narasumberKeyID.add(self.id)

        self.nama = r.choice(list(uniqueName))
        self.email = randomEmailName(self.nama)
        self.tempat = r.choice(list(uniqueLocation))
        self.tanggal = randomTimeStamp(0,3600)
        self.no_hp = r.randint(10000000000,99999999999)
        self.jumlah_berita = r.randint(0,50) #arbitrary number, harusnya ini digenerate sama DBMSnya tapi why not
        self.rerata_kata = r.randint(0,5000) #arbitrary number, harusnya ini digenerate sama DBMSnya tapi why not
        self.id_universitas = r.choice(list(universitas.universitasKeyID)) #foreign key to universitas

    def generateID(self):
        genID = r.randint(1,2000000)
        while genID in narasumber.narasumberKeyID:
            genID = r.randint(1,2000000)
        return genID
    
    def __str__(self):
        return stringifyTable(narasumber.tableName, self.id, self.nama, self.email, self.tempat, self.tanggal, self.no_hp, self.jumlah_berita, self.rerata_kata, self.id_universitas)

class rating: #Final
    ratingKeyID = set() #(id_universitas, ip_address)
    tableName = "rating"

    def __init__(self):
        self.id_universitas = r.choice(list(universitas.universitasKeyID)) #PK dan foreign key to universitas
        self.ip_address = self.generateIP() #PK
        rating.ratingKeyID.add((self.id_universitas, self.ip_address))

        self.nilai = round(r.uniform(0,10), 2)

    def generateIP(self):
        genIP = r.choice(list(uniqueIP))
        while (self.id_universitas, genIP) in rating.ratingKeyID:
            genIP = r.choice(list(uniqueIP))
        return genIP
    
    def __str__(self):
        return stringifyTable(rating.tableName, self.id_universitas, self.ip_address, self.nilai)
            
class narasumber_berita: #Final
    narasumber_beritaKeyID = set() #format is (urlBerita, idNarasumber)
    tableName = "narasumber_berita"

    def __init__(self):
        keyGen = self.generatePrimaryKey()
        self.url_berita = keyGen[0]
        self.id_narasumber = keyGen[1]
        narasumber_berita.narasumber_beritaKeyID.add((self.url_berita, self.id_narasumber))
    
    def generatePrimaryKey(self):
        genBerita = r.choice(list(berita.beritaKeyURL))
        genNarasumber = r.choice(list(narasumber.narasumberKeyID))
        while (genBerita, genNarasumber) in narasumber_berita.narasumber_beritaKeyID:
            genBerita = r.choice(list(berita.beritaKeyURL))
            genNarasumber = r.choice(list(narasumber.narasumberKeyID))
        return (genBerita, genNarasumber)
    
    def __str__(self):
        return stringifyTable(narasumber_berita.tableName, self.url_berita, self.id_narasumber)

class komentar: #FINAL
    komentarKeyID = set()
    tableName = "komentar"

    def __init__(self):
        self.id = self.generateID()
        komentar.komentarKeyID.add(self.id)
        
        randomTimeStampHere = datetime.datetime.now() + datetime.timedelta(minutes = 3600)
        self.tanggal = randomTimeStampHere.strftime("%m-%d-%y")
        self.jam = randomTimeStampHere.strftime("%H:%M:%S")
        self.konten = r.choice(list(uniqueLoremIpsum))
        self.nama_user = r.choice(list(uniqueName))
        self.email_user = randomEmailName(self.nama_user)
        self.url_user = randomString(40)
        self.url_berita = r.choice(list(berita.beritaKeyURL)) #Foreign key ke berita
    
    def generateID(self):
        genID = r.randint(0,2000000)
        while genID in komentar.komentarKeyID:
            genID = r.randint(0,2000000)
        return genID
    
    def __str__(self):
        return stringifyTable(komentar.tableName, self.id, self.tanggal, self.jam, self.konten, self.nama_user, self.email_user, self.url_user, self.url_berita)

class polling: #FINAL - ish
    pollingKeyID = set()
    startMinuteMark = 3600
    tableName = "polling"

    def __init__(self):
        self.id = self.generateID()
        polling.pollingKeyID.add(self.id)

        self.polling_start = randomTimeStamp(0,self.startMinuteMark)
        self.polling_end = randomTimeStamp(self.startMinuteMark, self.startMinuteMark * 10)
        self.total_responden = r.randint(0,2000000) #adalah total dari orang2 yang menunjuk ke key Polling dari Responden. Here, you could do whatever
    
    def generateID(self):
        genID = r.randint(1,2000000)
        while genID in polling.pollingKeyID:
            genID = r.randint(1,2000000)
        return genID
    
    def __str__(self):
        return stringifyTable(polling.tableName, self.id, self.polling_start, self.polling_end, self.total_responden)


class polling_berita: #FINAL
    tableName = "polling_berita"
    polling_beritaKeyPolling = set() #key polling saja karena relasinya satu berita memiliki banyak polling bukan sebaliknya
    
    def __init__(self):
        self.id_polling = self.keyGen() #PK
        polling_berita.polling_beritaKeyPolling.add(self.id_polling)

        self.url_berita = r.choice(list(berita.beritaKeyURL))
    
    def keyGen(self): #BUG: INNEFICIENCY genpolling bakal makan waktu lama buat cari pollingkeyID yang belum terpakai.
        genPolling = r.choice(list(polling.pollingKeyID))
        while genPolling in polling_berita.polling_beritaKeyPolling:
            genPolling = r.choice(list(polling.pollingKeyID))
        return genPolling

    def __str__(self):
        return stringifyTable(polling_berita.tableName, self.id_polling, self.url_berita)

class polling_biasa: #Final
    tableName = "polling_biasa"
    polling_biasaKey = set()
    def __init__(self):
        self.id_polling = self.keyGen() #PK
        polling_biasa.polling_biasaKey.add(self.id_polling)

        self.url = randomString(40)
        self.deskripsi = randomLoremIpsum(100)

    def keyGen(self):
        genID = r.choice(list(polling.pollingKeyID))
        while genID in polling_biasa.polling_biasaKey or genID in polling_berita.polling_beritaKeyPolling:
            genID = r.choice(list(polling.pollingKeyID))
        return genID

    def __str__(self):
        return stringifyTable(polling_biasa.tableName, self.id_polling, self.url, self.deskripsi)

class respon: #FINAL
    tableName = "respon"
    responKeyID = set() #(genID, genJawaban)

    def __init__(self):
        generatedKeys = self.generateKeys()
        self.id_polling = generatedKeys[0]
        self.jawaban = generatedKeys[1]
        respon.responKeyID.add(generatedKeys)

        self.jumlah_dipilih = r.randint(0,100000)

    def generateKeys(self):
        genID = r.choice(list(polling.pollingKeyID))
        genJawaban = randomLoremIpsum(40)
        while (genID, genJawaban) in respon.responKeyID:
            genID = r.choice(list(polling.pollingKeyID))
            genJawaban = randomLoremIpsum(40)
        return (genID, genJawaban)
    
    def __str__(self):
        return stringifyTable(respon.tableName, self.id_polling, self.jawaban, self.jumlah_dipilih)

class responden: #FINAL
    tableName = "responden"
    respondenKeyID = set() #(id_polling, ip_address)

    def __init__(self):
        genedKey = self.generateKey()
        self.id_polling = genedKey[0] #PK and FK to polling.id
        self.ip_address = genedKey[1] #PK
        responden.respondenKeyID.add(genedKey)
    
    def generateKey(self):
        genID = r.choice(list(polling.pollingKeyID))
        genIP = r.choice(list(uniqueIP))
        while (genID,genIP) in responden.respondenKeyID:
            genID = r.choice(list(polling.pollingKeyID))
            genIP = r.choice(list(uniqueIP))
        return (genID, genIP)
    
    def __str__(self):
        return stringifyTable(responden.tableName, self.id_polling, self.ip_address)

class honor: #FINAL
    tableName = "honor"
    honorKeyPair = set() #(id_narasumber, tgl_diberikan)

    def __init__(self):
        genedKey = self.generateKey()
        honor.honorKeyPair.add(genedKey)
        self.id_narasumber = genedKey[0] #PK and FK to narasumber.id
        self.tgl_diberikan = genedKey[1] #PK
        self.jumlah_berita = r.randint(0,1000) #arbitrary
        self.jumlah_gaji = r.randint(0,1000000)

    def generateKey(self):
        genID = r.choice(list(narasumber.narasumberKeyID))
        genTGL = randomTimeStamp(0,3600)
        while (genID, genTGL) in honor.honorKeyPair:
            genID = r.choice(list(narasumber.narasumberKeyID))
            genTGL = randomTimeStamp(0,3600)
        return (genID, genTGL)

    def __str__(self):
        return stringifyTable(honor.tableName, self.id_narasumber, self.tgl_diberikan, self.jumlah_berita, self.jumlah_gaji)

#All classes below is final
class dosen:
    tableName = "dosen"
    dosenKeyID = set()
    
    def __init__(self):
        self.idNarasumber = self.keyGen() #PK and FK to narasumber.id
        dosen.dosenKeyID.add(self.idNarasumber)

        self.nik_dosen = str(r.randint(1000000000000000000,9999999999999999999))
        self.jurusan = randomLoremIpsum(20).split(" ")[0]
    
    def keyGen(self):
        genID = r.choice(list(narasumber.narasumberKeyID))
        while genID in dosen.dosenKeyID:
            genID = r.choice(list(narasumber.narasumberKeyID))
        return genID
    
    def __str__(self):
        return stringifyTable(dosen.tableName, self.idNarasumber, self.nik_dosen, self.jurusan)

class staf:
    #PK gak boleh ada di dosen
    stafKeyID = set()
    tableName = "staf"

    def __init__(self):
        self.id_narasumber = self.genKey() #PK and FK to narasumber.id
        staf.stafKeyID.add(self.id_narasumber)
        self.nik_staf = str(r.randint(1000000000000000000,9999999999999999999))
        self.posisi = randomLoremIpsum(19)
    
    def genKey(self):
        genID = r.choice(list(narasumber.narasumberKeyID))
        while genID in staf.stafKeyID or genID in dosen.dosenKeyID:
            genID = r.choice(list(narasumber.narasumberKeyID))
        return genID
    
    def __str__(self):
        return stringifyTable(staf.tableName, self.id_narasumber, self.nik_staf, self.posisi)

class mahasiswa:
    #PK gak boleh ada di staf
    mahasiswaKeyID = set()
    tableName = "mahasiswa"

    def __init__(self):
        self.id_narasumber = self.keyGen() #PK and FK to narasumber.id
        mahasiswa.mahasiswaKeyID.add(self.id_narasumber)

        self.npm = str(r.randint(1000000000000000000,9999999999999999999))
        self.status = randomLoremIpsum(19)

    def keyGen(self):
        genID = r.choice(list(narasumber.narasumberKeyID))
        while genID in mahasiswa.mahasiswaKeyID or genID in staf.stafKeyID:
            genID = r.choice(list(narasumber.narasumberKeyID))
        return genID

    def __str__(self):
        return stringifyTable(mahasiswa.tableName, self.id_narasumber, self.npm, self.status)

class rekening:
    rekeningKeyNomor = set()
    tableName = "rekening"

    def __init__(self):
        self.nomor = self.keyGen()
        rekening.rekeningKeyNomor.add(self.nomor)

        self.nama_bank = randomLoremIpsum(19)
        self.id_narasumber = r.choice(list(narasumber.narasumberKeyID)) #FK to narasumber.id
    
    def keyGen(self):
        genNomor = str(r.randint(1000000000000000000,9999999999999999999))
        while genNomor in rekening.rekeningKeyNomor:
            genNomor = str(r.randint(1000000000000000000,9999999999999999999))
        return genNomor
    
    def __str__(self):
        return stringifyTable(rekening.tableName, self.nomor, self.nama_bank, self.id_narasumber)

class kupon: #Terisi otomatis oleh sistem
    pass

class riwayat:
    riwayatKeyPair = set() #(url_berita, id_riwayat)
    tableName = "riwayat"

    def __init__(self):
        genedKey = self.keyGen()
        self.url_berita = genedKey[0]
        self.id_riwayat = genedKey[1]
        riwayat.riwayatKeyPair.add(genedKey)

        self.waktu_revisi = randomTimeStamp(0, 3600)
    
    def keyGen(self):
        genURL = r.choice(list(berita.beritaKeyURL))
        genID = r.randint(0,2000000)
        while (genURL, genID) in riwayat.riwayatKeyPair:
            genURL = r.choice(list(berita.beritaKeyURL))
            genID = r.randint(0,2000000)
        return (genURL, genID)
    
    def __str__(self):
        return stringifyTable(riwayat.tableName, self.url_berita, self.id_riwayat, self.waktu_revisi)

class tag:
    tagKeyPair = set() #(url_berita, tag)
    tableName = "tag"
    
    def __init__(self):
        genedKey = self.keyGen()
        self.url_berita = genedKey[0]
        self.tag= genedKey[1]
        tag.tagKeyPair.add(genedKey)
    
    def keyGen(self):
        genURL = r.choice(list(berita.beritaKeyURL))
        genTag = randomLoremIpsum(40)
        while (genURL, genTag) in tag.tagKeyPair:
            genURL = r.choice(list(berita.beritaKeyURL))
            genTag = randomLoremIpsum(40)
        return (genURL, genTag)
    
    def __str__(self):
        return stringifyTable(tag.tableName, self.url_berita, self.tag)

#There's 19 tables in total
if __name__ == "__main__":
    
    #INIT Random data-set
    with open("RandomData/MOCK_NAME.csv") as f:
        temp = f.readline()
        while temp != "":
            if temp[-1] == "\n":
                newTemp = list(temp)
                newTemp[-1] = ""
                temp = "".join(newTemp)
            uniqueName.add(temp)
            temp = f.readline()

    with open("RandomData/MOCK_LOCATION.csv") as f:
        temp = f.readline()
        while temp != "":
            if temp[-1] == "\n":
                newTemp = list(temp)
                newTemp[-1] = ""
                temp = "".join(newTemp)
            uniqueLocation.add(temp)
            temp = f.readline()

    with open("RandomData/MOCK_WEBSITENAME.csv") as f:
        temp = f.readline()
        while temp != "":
            if temp[-1] == "\n":
                newTemp = list(temp)
                newTemp[-1] = ""
                temp = "".join(newTemp)
            uniqueWebsiteName.add(temp)
            temp = f.readline()

    with open("RandomData/MOCK_LOREMIPSUM.csv") as f:
        temp = f.readline()
        while temp != "":
            if temp[-1] == "\n":
                newTemp = list(temp)
                newTemp[-1] = ""
                temp = "".join(newTemp)
            uniqueLoremIpsum.add(temp)
            temp = f.readline()

    with open("RandomData/MOCK_IP.csv") as f:
        temp = f.readline()
        while temp != "":
            if temp[-1] == "\n":
                newTemp = list(temp)
                newTemp[-1] = ""
                temp = "".join(newTemp)
            uniqueIP.add(temp)
            temp = f.readline()

    #Print for unique data gotten from /RandomData/
    print("There are " + str(len(uniqueName)) + " different names to chose from")
    print("There are " + str(len(uniqueLocation)) + " random locations name to choose from")
    print("There are " + str(len(uniqueWebsiteName)) + " random websites name to choose from")
    print("There are " + str(len(uniqueIP)) + " random IP addresses name to choose from")
    print("There are " + str(len(uniqueLoremIpsum)) + " random lorem ipsum name to choose from")
    
    #Generating part
    generateRowsToString(textList, universitas, universitasCount)
    generateRowsToString(textList, berita, beritaCount)
    generateRowsToString(textList, narasumber, narasumberCount)
    generateRowsToString(textList, rating, ratingCount)
    generateRowsToString(textList, narasumber_berita, narasumber_beritaCount)
    generateRowsToString(textList, komentar, komentarCount)
    generateRowsToString(textList, polling, pollingCount)
    generateRowsToString(textList, polling_berita, polling_beritaCount)
    generateRowsToString(textList, polling_biasa, polling_biasaCount)
    generateRowsToString(textList, respon, responCount)
    generateRowsToString(textList, responden, respondenCount)
    generateRowsToString(textList, honor, honorCount)
    generateRowsToString(textList, dosen, dosenCount)
    generateRowsToString(textList, staf, stafCount)
    generateRowsToString(textList, mahasiswa, MahasiswaCount)
    generateRowsToString(textList, rekening, RekeningCount)
    generateRowsToString(textList, riwayat, riwayatCount)
    generateRowsToString(textList, tag, tagCount)
    

    #Writing to txt part
    theFinalString = ''.join(textList)
    with open(fileName, fileMode) as f:
        f.write(theFinalString)
    
    print("The file you need to use for insert query in on generated_input.txt")
    print("Press enter to exit")
    while True:
        a = input()
        if a != None:
            exit()

