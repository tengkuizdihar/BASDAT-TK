from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(label="Username", max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'mdl-textfield__input', 'type' :'text', 'id' : 'username', 'placeholder': 'Enter your username'}))
    password = forms.CharField(label="Password", max_length=50, required=True, widget=forms.TextInput(attrs={'class': 'mdl-textfield__input', 'type' :'password', 'id' : 'password', 'placeholder': 'Enter your password'}))

class RegisterForm(forms.Form):
    username = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your username',
    }
    password = {
        'type': 'password',
        'class': 'mdl-textfield__input',
        'placeholder': 'Minimal 8 characters',
    }
    nama = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your full name',
    }
    tempat_lahir = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your birth place',
    }
    tanggal_lahir = {
        'type': 'date',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your birth date',
    }
    email = {
        'type': 'email',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your valid email adress',
    }
    no_hp = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': '08xxxxxxxxxx',
    }
    role = {
        ('', '---------------'),
        ('dosen', 'Dosen'),
        ('staf', 'Staf'),
        ('mahasiswa', 'Mahasiswa'),
    }
    error_message = {
        'required': 'This field is required to fill',
    }
    nomor_identitas = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your NIK/NPM',
    }
    jurusan = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your major. Ex: Information System, etc.',
        'disabled':'true'
    }
    status = {
        ('', '---------------'),
        ('aktif', 'Aktif'),
        ('tidak', 'Tidak Aktif'),
    }
    posisi = {
        'type': 'text',
        'class': 'mdl-textfield__input',
        'placeholder': 'Enter your position. Ex: Analyst, etc.',
        'disabled':'true'
    }

    username = forms.CharField(label='Username', required=True, max_length=50, widget=forms.TextInput(attrs=username))
    password = forms.CharField(label='Password', required=True, max_length=50, widget=forms.TextInput(attrs=password))

    nama = forms.CharField(label='Nama Lengkap', required=True, max_length=50, widget=forms.TextInput(attrs=nama))
    email = forms.CharField(label='Email', required=True, max_length=50, widget=forms.TextInput(attrs=email))
    tempat = forms.CharField(label='Tempat Lahir', required=True, max_length=50, widget=forms.TextInput(attrs=tempat_lahir))
    tanggal = forms.DateTimeField(label='Tanggal Lahir', required=True, widget=forms.DateInput(attrs=tanggal_lahir))
    no_hp = forms.CharField(label='Nomor HP', max_length=50,required=False, widget=forms.TextInput(attrs=no_hp))
    nomor_identitas = forms.CharField(label='Nomor Identitas', required=True, max_length=20, widget=forms.TextInput(attrs=nomor_identitas))
    jurusan = forms.CharField(label='Jurusan', required=False, max_length=50, widget=forms.TextInput(attrs=jurusan))
    posisi = forms.CharField(label='Posisi', required=False, max_length=50, widget=forms.TextInput(attrs=posisi))

    role = forms.ChoiceField(label='Role', choices=role, required=True, widget=forms.Select(attrs={'class': 'mdl-textfield__input'}))
    status = forms.ChoiceField(label='Status Kemahasiswaan', required=False, choices=status, widget=forms.Select(attrs={'class': 'mdl-textfield__input','disabled':'true'}))
