from django.db import connection
import random as r

def checkRowAttribute(listOfDict, attribute, valueToBeChecked):
    for d in listOfDict:
        if d[attribute] == valueToBeChecked:
            return True

# Gunakan untuk mengambil semua row dari sebuah cursor
# Return all rows from a cursor as a dict
def dictfetchall(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

#Ini hanya contoh. Please jangan gunakan.
def my_custom_sql():
    with connection.cursor() as cursor:
        cursor.execute("UPDATE bar SET foo = 1 WHERE baz = %s", "test")
        cursor.execute("SELECT foo FROM basdat_rating WHERE baz = %s", "test")
        dictRow = dictfetchall(cursor)
        return dictRow

# Generate untuk ID polling
def generate_id_polling():
    with connection.cursor() as cursor:
        id_temp = r.randint(0, 2000000)
        cursor.execute("SELECT id FROM bmnc.polling where id=%s", str(id_temp))
        dictRow = dictfetchall(cursor)

    while len(dictRow) > 0:
        with connection.cursor() as cursor:
            id_temp = r.randint(0, 2000000)
            cursor.execute("SELECT id FROM bmnc.polling where id=%s", str(id_temp))
            dictRow = dictfetchall(cursor)

    return id_temp

def berita_url_exist(url):
    with connection.cursor() as cursor:
        cursor.execute("SELECT url FROM bmnc.berita where url=%s", url)
        dictRow = dictfetchall(cursor)
        return len(dictRow) > 0

def test():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM bmnc.universitas")
        dictRow = dictfetchall(cursor)
        return dictRow

def check_username_json(username):
    with connection.cursor() as cursor:
        cursor.execute('select count(*) from narasumber')
        count = cursor.fetchone()[0] #(2,)
        taken = False
        if(count>0):
            cursor.execute('select username from narasumber')
            username_users = cursor.fetchall()
            print(username_users[0][0])
            for p in username_users:
                print(str(p[0])+" dan "+str(username))
                if p[0] == username:
                    taken = True
                    break
                else:
                    taken = False
        else:
            taken = False
        data = {
            'uname_is_taken' : taken
        }
    return data

def check_npm(nomor_identitas, role):
    with connection.cursor() as cursor:
        taken = False
        no_role = False
        if role == 'dosen':
            cursor.execute('select count(*) from bmnc.dosen')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select nik_dosen from bmnc.dosen')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False
        elif role == 'mahasiswa':
            cursor.execute('select count(*) from bmnc.mahasiswa')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select npm from bmnc.mahasiswa')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False

        elif role == 'staf':
            cursor.execute('select count(*) from bmnc.staf')
            count = cursor.fetchone()[0]
            if(count>0):
                print("cek nik dosen")
                cursor.execute('select nik_staf from bmnc.staf')
                cek_npm = cursor.fetchall()
                print(cek_npm[0][0])
                for p in cek_npm:
                    print(str(p[0])+" dan "+str(nomor_identitas))
                    if p[0] == nomor_identitas:
                        taken = True
                        break
                    else:
                        taken = False
            else:
                taken = False
        else:
            no_role = True
    data = {
        'uname_is_taken' : taken,
        'cek_role' : no_role
    }
    return data

def get_id_universitas():
    with connection.cursor() as cursor:
        cursor.execute("SELECT id FROM universitas")
        result = dictfetchall(cursor)
        returnThis = []
        for i in result:
            returnThis.append(i['id'])
        return result

# Generate a new narasumber id. Save to use even if the new "narasumber" is not inserted in.
def generate_id_narasumber():
    with connection.cursor() as cursor:
        cursor.execute('select count(*) from narasumber')
        count = int(cursor.fetchone()[0])
        id_narasumber = 0
        if count > 0:
            id_narasumber = count + 1
        return id_narasumber

# Check if username is already registrated.
def check_username(username):
    with connection.cursor() as cursor:
        cursor.execute("SELECT username FROM bmnc.userlist WHERE username = %s", username)
        result = dictfetchall(cursor)
        return len(result) > 0

# Jadikan ini saja authnya. Lebih simpel.
def user_auth(username, password):
    with connection.cursor() as cursor:
        cursor.execute("SELECT username,password FROM bmnc.userlist WHERE username = %s and password = %s", [username, password])
        result = cursor.fetchone()
        if result['username'] == username and result['password'] == password:
            return True
        return False

def get_user_universitas(username):
    with connection.cursor() as cursor:
        cursor.execute("SELECT uni.id as iduni FROM bmnc.universitas as uni, bmnc.narasumber as n, bmnc.userlist as user WHERE user.username=%s and id_narasumber=n.id and id_universitas=uni.id", username)
        return dictfetchall(cursor)[0]['iduni']

def insert_berita(url, judul, topik, todayDate, jumlahKata, id_universitas):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO bmnc.berita VALUES (%s,%s,%s,%s,%s,%s,0,%s)",[url, judul, topik, todayDate, todayDate, jumlahKata, id_universitas])

def insert_polling_biasa(id, polling_start, polling_end, url, deskripsi):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO bmnc.polling VALUES (%s, %s, %s, 0)", [id, polling_start, polling_end])
        cursor.execute("INSERT INTO bmnc.polling_biasa VALUES (%s, %s, %s)", [id, url, deskripsi])

def insert_polling_berita(id, polling_start, polling_end, url_berita):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO bmnc.polling VALUES (%s, %s, %s, 0)", [id, polling_start, polling_end])
        cursor.execute("INSERT INTO bmnc.polling_berita VALUES (%s, %s)", [id, url_berita])

def insert_jawaban(id_polling, jawaban):
    with connection.cursor() as cursor:
        cursor.execute("INSERT INTO bmnc.respon VALUES (%s, %s, 0)", [id_polling, jawaban])