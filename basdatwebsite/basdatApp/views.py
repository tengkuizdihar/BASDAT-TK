from django.shortcuts import render, redirect, reverse
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .database_things import *
from .forms import *
from datetime import date

response = {}

# Create your views here. Render stuff here too.
def index(request):
    return render(request, 'home/home.html', response)

def login_page(request):
    response['tes'] = "Tes"
    response['login_form'] = LoginForm
    print("masuk")
    return render(request, 'login/login.html', response)

def register_page(request):
    response['cek'] = "Halaman register"
    response['reg_form'] = RegisterForm
    form = RegisterForm
    print(form.errors)
    return render(request, 'login/regis.html', response)

def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

@csrf_exempt
def validate_pwd(request):
    if request.method == "POST":
        password = request.POST.get('password', False)
        notif = ""
        list = []
        length = len(password)
        if (length<8):
            notif = "Password minimal harus 8 karakter"
            list.append(notif)
        if (hasNumbers(password)==False):
            notif = "Password harus mengandung angka"
            list.append(notif)
        print(list)
        data = {
            'list_of_notif' : list
        }
        return JsonResponse(data)

@csrf_exempt
def check_username_view(request):
    if request.method == 'POST':
        username = request.POST.get('username', False)
        data = check_username_json(username)
        return JsonResponse(data)

@csrf_exempt
def check_npm_view(request):
    if request.method == 'POST':
        nomor_identitas = request.POST.get('npm', False)
        role = request.POST.get('role', False)
        data = check_npm(role)
        return JsonResponse(data)

@csrf_exempt
def login(request):
    print("masuk form")
    form = LoginForm(data=request.POST)
    print(form.errors)
    
    if (request.method == 'POST' and form.is_valid()):
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        exist = check_username(username)
        auth = user_auth(username, password)

        if not exist:
            messages.error(request, "Anda belum mendaftar")
            return HttpResponseRedirect(reverse('basdatApp:login'))
        
        elif auth:
            request.session['username'] = username
            request.session['is_login'] = "True"
            request.session['id'] = get_id[0]
            messages.success(request, "Anda berhasil login")
            return HttpResponseRedirect(reverse('basdatApp: index'))
        
        elif not auth:
            messages.error(request, "Username atau password salah")
            return HttpResponseRedirect(reverse('basdatApp:login'))

    else:
        return HttpResponse("null")

@csrf_exempt
def register_narasumber(request):
    print("masuk form")
    form = RegisterForm(data=request.POST)
    print("hai")
    print(request.method)
    print(form.is_valid())
    print(form.errors)
    
    if (request.method == 'POST' and form.is_valid()):
        id_narasumber = generate_id_narasumber()
        
        username = request.POST.get('username', False)
        password = request.POST.get('password', False)
        nama = request.POST.get('nama', False)
        role = request.POST.get('role', False)
        nomor_identitas = request.POST.get('nomor_identitas', False)
        email = request.POST.get('email', False)
        no_hp = request.POST.get('no_hp', False)
        tempat = request.POST.get('tempat', False)
        tanggal = request.POST.get('tanggal', False)
        status = request.POST.get('status', False)
        jurusan = request.POST.get('jurusan', False)
        posisi = request.POST.get('posisi', False)
        
        print(status)
        id_universitas = request.POST.get('id_universitas', False)

        query_mhs_lg = "insert into bmnc.MAHASISWA (id_narasumber,npm,status) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(status)+"')"
        query_dosen = "insert into bmnc.DOSEN (id_narasumber,nik_dosen,jurusan) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(jurusan)+"')"
        query_staf = "insert into bmnc.STAF (id_narasumber,nik_staf,posisi) values ("+str(id_narasumber)+", "+"'"+str(nomor_identitas)+"'"+", "+"'"+str(posisi)+"')"
        print(query_mhs_lg)
        print(query_dosen)
        print(query_staf)

        if role in ('dosen','staf','mahasiswa'):
            print(id_universitas)
            query = "insert into bmnc.NARASUMBER (id, nama, email, tempat, tanggal, no_hp, id_universitas, username, password) values ("+str(id_narasumber)+", "+"'"+str(nama)+"'"+", "+"'"+str(email)+"'"+", "+"'"+str(tempat)+"'"+", "+"'"+str(tanggal)+"'"+", "+"'"+str(no_hp)+"'"+", "+str(id_universitas)+", "+"'"+str(username)+"'"+", "+"'"+str(password)+"')"
            print(query)
            cursor.execute(query)

        # Insert operation mulai
        cursor = connection.cursor()
        if role == 'mahasiswa':
            cursor.execute(query_mhs_lg)
        elif role == 'dosen':
            cursor.execute(query_dosen)
        elif role == 'staf':
            cursor.execute(query_staf)
        cursor.close()
        # Insert op selesai

        messages.success(request, "Selamat! Anda berhasil mendaftar! Silakan login kembali untuk melanjutkan")
        return HttpResponseRedirect(reverse('basdatApp:login'))
    else:
        return HttpResponse("null")

def getUniversitas(request):
    if request.method == 'GET':
        return JsonResponse(get_id_universitas(), safe=False)

def logout(request):
    request.session['login'] = False
    try:
        request.session.flush()
    except KeyError:
        pass
    messages.info(request, "Anda berhasil logout.")
    return HttpResponseRedirect(reverse('basdatApp:index'))

# KODE MULAI DARI SINI DIHAR, gua komen gini biar gak lupa, mohon maklum
def berita_create(request):
    response['warning'] = False
    return render(request, 'berita/berita_create.html', response)

def polling_berita_create(request):
    response['warning'] = False
    return render(request, 'polling/polling_berita_create.html', response)

def polling_create(request):
    response['warning'] = False
    return render(request, 'polling/polling_create.html', response)

# Test if the DB connect or not
def testDBConnect(request):
    print(test())
    print("PRINTED THE ENTIRE ID OF UNIVERSITAS")
    return redirect("basdatApp:index")

# response['warning'] adalah boolean untuk render warning atau tidak
# response['warning_message'] untuk pesan dari warning
# TODO make auth here
def berita_create_func(request):
    # if request.session.get('login') == 'True':
    #     ====di sini kode post di bawah====
    # else:
    #     return render(request, 'berita/berita_create.html', response)
    
    if request.method == "POST":
        url = request.POST.get("url")
        judul = request.POST.get("judul")
        topik = request.POST.get("topik")
        jumlahKata = request.POST.get("jumlahKata")
        tag = reques.POST.get("tag")
        todayDate = date.today().isoformat()
        id_universitas_user = get_user_universitas(request.session.get("username"))
        
        insert_berita(url, judul, topik, todayDate, jumlahKata, id_universitas_user)
        
        return redirect("basdatApp:berita_create")

        
        
# response['warning'] adalah boolean untuk render warning atau tidak
# response['warning_message'] untuk pesan dari warning
# TODO make auth here
def polling_create_func(request):
    # if request.session.get('login') == 'True':
    #     ====di sini kode post di bawah====
    # else:
    #     return render(request, 'polling/polling_create.html', response)

    if request.method == "POST":
        id = generate_id_polling()
        url_mock = "iniUrlMock"
        deskripsi = request.POST.get("deskripsi")
        waktuMulai = request.POST.get("waktuMulai")
        waktuSelesai = request.POST.get("waktuSelesai")

        print(waktuSelesai)
        
        # insert for polling here
        insert_polling_biasa(id, waktumulai, waktuSelesai, url_mock, deskripsi)

        for key, value in request.POST.items():
            if key[:10] == "pertanyaan":
                insert_jawaban(id, value)
    return render(request, 'polling/polling_create.html', response)

# response['warning'] adalah boolean untuk render warning atau tidak
# response['warning_message'] untuk pesan dari warning
# TODO make auth here
def polling_berita_create_func(request):
    # if request.session.get('login') == 'True':
    #     ====di sini kode post di bawah====
    # else:
    #     return render(request, 'polling/polling_berita_create.html', response)
        
    if request.method == "POST":
        id = generate_id_polling()
        url = request.POST.get("url")
        waktuMulai = request.POST.get("waktuMulai")
        waktuSelesai = request.POST.get("waktuSelesai")

        print(waktuMulai)
        if url:
            if not berita_url_exist(url):
                response['warning'] = True
                response['warning_message'] = "URL doesn't exist!"
                return render(request, 'polling/polling_berita_create.html', response)

        # insert_polling_berita(id, waktuMulai, waktuSelesai, url)

        for key, value in request.POST.items():
            if key[:10] == "pertanyaan":
                insert_jawaban(id, value)
        return render(request, 'polling/polling_berita_create.html', response)

# END KODE BUATAN DIHAR

# TODO benerin ini, cursornya salah
# Benerin ini "jumlah" kayaknya gak guna deh
def profile(request):
    cursor = connection.cursor()
    cursor.execute('select count(*) from narasumber')
    jumlah = cursor.fetchone()[0]
    if jumlah != 0:
        #narsum = request.session['id']
        query = 'select nama from narasumber where id=1'
        cursor.execute(query)
        nama = cursor.fetchone()[0]
        response['nama'] = nama

        #query = 'select username from narasumber where id='+str(narsum)
        #cursor.execute(query)
        #username = cursor.fetchone()[0]
        #response['username'] = username

        query1 = 'select id_narasumber from mahasiswa where id_narasumber=1'
        #query2 = 'select id_narasumber from dosen where id_narasumber='+str(narsum)
        #query3 = 'select id_narasumber from staf where id_narasumber='+str(narsum)

        role = ''
        cursor.execute(query1)
        q = cursor.fetchone()
        if(q is not None):
            role = 'Mahasiswa'
            response['nama_nomor'] = 'NPM'
            query = 'select npm from mahasiswa where id_narasumber=1'
            cursor.execute(query)
            number = cursor.fetchone()[0]
            print (number)
            response['number'] = number

        #cursor.execute(query2)
        q = cursor.fetchone()
        if(q is not None):
            role = 'Dosen'
            response['nama_nomor'] = 'NIK'
            query = 'select nik_dosen from dosen where id_narasumber=1'
            cursor.execute(query)
            number = cursor.fetchone()[0]
            print (number)
            response['number'] = number

        #cursor.execute(query3)
        q = cursor.fetchone()
        if(q is not None):
            role = 'Staff'
            response['nama_nomor'] = 'NIK'
            query = 'select nik_staf from staf where id_narasumber=1'
            cursor.execute(query)
            number = cursor.fetchone()[0]
            print (number)
            response['number'] = number

        response['role'] = role

        query = 'select email from narasumber where id=1'
        cursor.execute(query)
        email = cursor.fetchone()[0]
        response['email'] = email

        query = 'select no_hp from narasumber where id=1'
        cursor.execute(query)
        phone = cursor.fetchone()[0]
        response['nomorhp'] = phone

        query = "select to_char(tanggal, "+"'"+"DD Month YYYY"+"'"+") as date from narasumber where id=1"
        cursor.execute(query)
        birthday = cursor.fetchone()[0]
        response['birthday'] = birthday

        query = 'select judul, url, created_at, updated_at, topik, jumlah_kata from berita, narasumber_berita, narasumber where id_narasumber = id and url_berita = url and id=1'
        cursor.execute(query)
        berita = cursor.fetchall()
        print(berita)
        
        response['news'] = berita
        return render(request, 'profile/profile.html', response)

def profile_html(request):
    return render(request, 'profile/profile.html', response)

def polling_list_html(request):
    return render(request, 'profile/polling_list.html', response)

def articles_list_html(request):
    return render(request, 'profile/articles_list.html', response)
