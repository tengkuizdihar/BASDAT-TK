from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^login-page/$', login_page, name='login'),
    url(r'^login/$', login, name='login-data'),
    url(r'^register/$', register_page, name='register'),
    url(r'^register/submit/$', register_narasumber, name='register_narasumber'),
    url(r'^validate-username/$', check_username_view, name='validate-username'),
    url(r'^validate-npm/$', check_npm_view, name='validate-npm'),
    url(r'^validate-pwd/$', validate_pwd, name='validate-pwd'),
    url(r'^getunivids/', getUniversitas, name='getunivids'),
    url(r'^logout/$', logout, name='logout'),
    url(r'polling-create/$', polling_create, name="polling_create"),
    url(r'^polling-create-func/$',  polling_create_func, name="polling_create_func"),
    url(r'polling-berita-create/$', polling_berita_create, name="polling_berita_create"),
    url(r'polling-berita-create-func/$', polling_berita_create_func, name="polling_berita_create_func"),
    url(r'berita-create/$', berita_create, name="berita_create"),
    url(r'berita-create-func/$', berita_create_func, name="berita_create_func"),
    url(r'testDBConnect/$', testDBConnect, name="testDBConnect"),
    url(r'profile_html/$', profile, name="profile_html"),
    url(r'polling_list_html/$', polling_list_html, name="polling_list_html"),
    url(r'articles_list_html/$', articles_list_html, name="articles_list_html"),
]
