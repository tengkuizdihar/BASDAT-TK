from flask import Flask, render_template, g
from flask_bootstrap import Bootstrap

def create_app(configfile = None):
    app = Flask(__name__)
    Bootstrap(app)
    
    @app.route('/')
    def home_page():
        print('bootstrap used')
        print('Ayy we made it')
        return render_template('home/home.html')

    @app.route('/error-login/')
    def error_login():
        return render_template('login/error.html')

    @app.route('/login/')
    def login_page():
        return render_template('login/login.html')

    @app.route('/regis/')
    def regis_page():
        return render_template('login/regis.html')
    
    @app.route('/berita-create/')
    def berita_create():
        return render_template('berita/berita_create.html')

    @app.route('/polling-berita-create/')
    def polling_berita_create():
        return render_template('polling/polling_berita_create.html')

    @app.route('/polling-create/')
    def polling_create():
        return render_template('polling/polling_create.html')

    @app.route('/articles-list/')
    def articles_list():
        return render_template('profile/articles_list.html')

    @app.route('/polling-list/')
    def polling_list():
        return render_template('profile/polling_list.html')

    @app.route('/profile/')
    def profile_page():
        return render_template('profile/profile.html')
    
    return app
    
if __name__ == '__main__':
    create_app().run(debug=True)
