# How to run
1. Open CMD on this directory
2. If you are on windows, use `set FLASK_APP=helloBasdat.py` to set environment variable.
3. Run the thing on the cmd by typing `flask run`
4. Enjoy.

# How to run, but with brains
1. Open cmd on this directory
2. Type run.bat
3. Enjoy.

# Changelog
* 0.0.1
    * Released the first working environment so you guys could make stuff here.
* Fixed activate.bat and rest in readme
    * Also added material design, added the demo for it to show that it's working. Extend the base.html in html to use it.
